import React from "react";
import Weather from "./components/weather";

const App = () => {
  return (
    <div>
      <h1>Weather App</h1>
      <Weather />
    </div>
  );
};

export default App;
